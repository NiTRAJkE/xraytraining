#!/usr/bin/env python
# coding: utf-8

# In[125]:


import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'
import cv2
import PIL
import os, sys
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from copy import deepcopy
from tqdm import tqdm


# In[126]:


data = pd.read_csv('../../Data/vinbigdata/train.csv')
print(len(data), data.keys())


# In[127]:


data['class_name'] = data['class_name'].apply(lambda x: 'Effusion' if x == 'Pleural effusion' else 'Infiltrate' if x == 'Infiltration' else 'Pneumonia' if x == 'Consolidation' else x)


# In[128]:


data = data[data['class_name'].isin(['Atelectasis', 'Cardiomegaly', 'Effusion', 'Infiltrate', 'Nodule/Mass', 'Pneumonia', 'Pneumothorax'])]
print(data['class_name'].value_counts())
print(len(data))


# In[129]:


CLASSES = {k: i for i, k in enumerate(sorted(data['class_name'].unique()))}
for c in CLASSES:
    print(f'{CLASSES[c]}: {c}')


# In[130]:


data['image'] = data['image_id'].apply(lambda x: x + '.png')
data['label'] = data['class_name'].apply(lambda x: CLASSES[x])
data['x'] = data['x_min']
data['y'] = data['y_min']
data['w'] = data['x_max'] - data['x_min']
data['h'] = data['y_max'] - data['y_min']
data['image_width'] = data['width']
data['image_height'] = data['height']


# In[124]:


data = data[data['class_name'].isin(['Cardiomegaly', 'Infiltrate', 'Nodule/Mass'])]
print(data['class_name'].value_counts())
print(len(data))


# In[131]:


data = data.drop([k for k in data.keys() if k not in ['image', 'label', 'x', 'y', 'w', 'h', 'patient_id', 'image_width', 'image_height']], axis=1)


# In[182]:


print(len(data))


# In[132]:


def split(data, val_size=0.15, test_size=0.15):
    temp_data = data.drop_duplicates('image')
    train_data, temp_data = train_test_split(temp_data, test_size=val_size + test_size, stratify=temp_data['label'], random_state=42)
    val_data, test_data = train_test_split(temp_data, test_size=test_size / (test_size+val_size), stratify=temp_data['label'], random_state=42)
    train_data = data[data['image'].isin(train_data['image'])]
    val_data = data[data['image'].isin(val_data['image'])]
    test_data = data[data['image'].isin(test_data['image'])]
    return train_data, val_data, test_data


# In[133]:


train_df, val_df, test_df = split(data)


# In[137]:


assert len(set(train_df['image']).intersection(set(val_df['image']))) == 0
assert len(set(train_df['image']).intersection(set(test_df['image']))) == 0
assert len(set(val_df['image']).intersection(set(test_df['image']))) == 0


# In[180]:


def split_hist(df, save_path=None):
    a_classes = {CLASSES[k]: k for k in CLASSES}
    dfl = df['label'].apply(lambda x: a_classes[x])
    dfl.value_counts()[sorted(dfl.unique())].plot(kind='bar')
    plt.xticks(rotation=15)
    plt.rcParams['figure.figsize'] = [8, 8]
    if save_path is not None:
        plt.savefig(save_path)
    else:
        plt.show()


# In[150]:


split_hist(train_df)
split_hist(val_df)
split_hist(test_df)


# In[181]:


split_hist(pd.concat([train_df, val_df, test_df]), save_path='vinbigdata.png')


# In[ ]:





# In[85]:


# TODO: MAKE .yaml dataset file


# In[82]:


val_df.head()


# In[138]:


def prepare_data(df, split):
    df['x'] = df['x'] / df['image_width']
    df['w'] = df['w'] / df['image_width']
    df['y'] = df['y'] / df['image_height']
    df['h'] = df['h'] / df['image_height']
    
    df['x_center'] = df['x'] + (df['w'] / 2)
    df['y_center'] = df['y'] + (df['h'] / 2)
    
    img_path = '../../Data/vinbigdata/images/'
    image_group = df.groupby('image')
    
    for image in tqdm(df['image'].unique()):
        cur_df = image_group.get_group(image)
        lines = []
        for i in range(len(cur_df)):
            lines.append(f'{cur_df["label"].iloc[i]} {cur_df["x_center"].iloc[i]} {cur_df["y_center"].iloc[i]} {cur_df["w"].iloc[i]} {cur_df["h"].iloc[i]}\n')
        lines[-1] = lines[-1][:-1]
        os.makedirs(f'../../Data/vinbigdata/labels/', exist_ok=True)
        with open(f'../../Data/vinbigdata/labels/{image.split(".")[0]}.txt', 'w') as file:
            file.writelines(lines)

    images = [f'./images/{s}' + '\n' for s in df['image'].unique()]
    images[-1] = images[-1][:-1]

    with open(f'../../Data/vinbigdata/{split}.txt', 'w') as file:
        file.writelines(images)


# In[139]:


prepare_data(val_df, 'val')
prepare_data(train_df, 'train')
prepare_data(test_df, 'test')


# In[ ]:





# In[140]:


data.to_csv('../../Data/vinbigdata/boxes.csv', index=False)


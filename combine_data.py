#!/usr/bin/env python
# coding: utf-8

# In[34]:


import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'
import cv2
import PIL
import os, sys
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from copy import deepcopy
from tqdm import tqdm


# In[35]:


train_lines = []
val_lines = []

with open('../../Data/vinbigdata/train.txt', 'r') as file:
    cur_lines = file.readlines()
    if not cur_lines[-1].endswith('\n'):
        cur_lines[-1] = cur_lines[-1] + '\n'
    cur_lines = ['./vinbigdata/images/' + s.split('/')[-1] for s in cur_lines]
    train_lines += cur_lines

with open('../../Data/vinbigdata/val.txt', 'r') as file:
    cur_lines = file.readlines()
    if not cur_lines[-1].endswith('\n'):
        cur_lines[-1] = cur_lines[-1] + '\n'
    cur_lines = ['./vinbigdata/images/' + s.split('/')[-1] for s in cur_lines]
    train_lines += cur_lines

with open('../../Data/ChestXRay/train.txt', 'r') as file:
    cur_lines = file.readlines()
    if not cur_lines[-1].endswith('\n'):
        cur_lines[-1] = cur_lines[-1] + '\n'
    cur_lines = ['./ChestXRay/images/' + s.split('/')[-1] for s in cur_lines]
    train_lines += cur_lines

with open('../../Data/ChestXRay/val.txt', 'r') as file:
    cur_lines = file.readlines()
    if not cur_lines[-1].endswith('\n'):
        cur_lines[-1] = cur_lines[-1] + '\n'
    cur_lines = ['./ChestXRay/images/' + s.split('/')[-1] for s in cur_lines]
    val_lines += cur_lines

train_lines[-1] = train_lines[-1][:-1]
val_lines[-1] = val_lines[-1][:-1]

print('Train dataset len:', len(train_lines))
print('Val dataset len:', len(val_lines))


# In[36]:


train_lines[-1]


# In[39]:


print(len(train_lines))
print(len(val_lines))


# In[37]:


with open('../../Data/train.txt', 'w') as file:
    file.writelines(train_lines)
with open('../../Data/val.txt', 'w') as file:
    file.writelines(val_lines)


# In[ ]:





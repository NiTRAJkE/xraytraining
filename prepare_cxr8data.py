#!/usr/bin/env python
# coding: utf-8

# In[36]:


import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'
import cv2
import PIL
import os, sys
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from copy import deepcopy
from tqdm import tqdm


# In[37]:


data = pd.read_csv('../../Data/ChestXRay/Data_Entry_2017_v2020.csv')
print(len(data), data.keys())


# In[38]:


boxes = pd.read_csv('../../Data/ChestXRay/BBox_List_2017.csv')
print(len(boxes), boxes.keys())


# In[39]:


boxes['Finding Label'] = boxes['Finding Label'].apply(lambda x: 'Nodule/Mass' if x in ['Nodule', 'Mass'] else x)


# In[40]:


CLASSES = {k: i for i, k in enumerate(sorted(boxes['Finding Label'].unique()))}
for c in CLASSES:
    print(f'{CLASSES[c]}: {c}')


# In[41]:


boxes = boxes.merge(data, 'left', on='Image Index')


# In[42]:


boxes['image'] = boxes['Image Index']
boxes['label'] = boxes['Finding Label'].apply(lambda x: CLASSES[x])
boxes['x'] = boxes['Bbox [x']
boxes['h'] = boxes['h]']
boxes['patient_id'] = boxes['Patient ID']
boxes['image_size'] = boxes['image'].apply(lambda path: PIL.Image.open('../../Data/ChestXRay/images/' + path).size)
boxes['image_width'] = boxes['image_size'].apply(lambda x: x[0])
boxes['image_height'] = boxes['image_size'].apply(lambda x: x[1])


# In[43]:


boxes = boxes.drop([k for k in boxes.keys() if k not in ['image', 'label', 'x', 'y', 'w', 'h', 'patient_id', 'image_width', 'image_height']], axis=1)


# In[63]:


print(len(boxes))


# In[44]:


def split(data, val_size=0.15, test_size=0.15):
    temp_data = data.drop_duplicates('patient_id')
    train_data, temp_data = train_test_split(temp_data, test_size=val_size + test_size, stratify=temp_data['label'], random_state=42)
    val_data, test_data = train_test_split(temp_data, test_size=test_size / (test_size+val_size), stratify=temp_data['label'], random_state=42)
    train_data = data[data['patient_id'].isin(train_data['patient_id'])]
    val_data = data[data['patient_id'].isin(val_data['patient_id'])]
    test_data = data[data['patient_id'].isin(test_data['patient_id'])]
    return train_data, val_data, test_data


# In[45]:


train_df, val_df, test_df = split(boxes)


# In[61]:


def split_hist(df, save_path=None):
    a_classes = {CLASSES[k]: k for k in CLASSES}
    dfl = df['label'].apply(lambda x: a_classes[x])
    dfl.value_counts()[sorted(dfl.unique())].plot(kind='bar', figsize=(8, 8))
    plt.xticks(rotation=15)
    plt.rcParams['figure.figsize'] = [8, 8]
    if save_path is not None:
        plt.savefig(save_path)
    else:
        plt.show()


# In[47]:


split_hist(train_df)
split_hist(val_df)
split_hist(test_df)


# In[62]:


split_hist(pd.concat([train_df, val_df, test_df]), save_path='cxr.png')


# In[48]:


for i in range(len(train_df)):
    if train_df['patient_id'].iloc[i] in pd.concat([val_df, test_df])['patient_id'].unique():
        print(i)
        break
for i in range(len(val_df)):
    if val_df['patient_id'].iloc[i] in test_df['patient_id'].unique():
        print(i)
        break


# In[ ]:





# In[85]:


# TODO: MAKE .yaml dataset file


# In[15]:


val_df.head()


# In[49]:


def prepare_data(df, split):
    df['x'] = df['x'] / df['image_width']
    df['w'] = df['w'] / df['image_width']
    df['y'] = df['y'] / df['image_height']
    df['h'] = df['h'] / df['image_height']
    
    df['x_center'] = df['x'] + (df['w'] / 2)
    df['y_center'] = df['y'] + (df['h'] / 2)
    
    img_path = '../../Data/ChestXRay/images/all/'
    image_group = df.groupby('image')
    
    for image in tqdm(df['image'].unique()):
        cur_df = image_group.get_group(image)
        lines = []
        for i in range(len(cur_df)):
            lines.append(f'{cur_df["label"].iloc[i]} {cur_df["x_center"].iloc[i]} {cur_df["y_center"].iloc[i]} {cur_df["w"].iloc[i]} {cur_df["h"].iloc[i]}\n')
        lines[-1] = lines[-1][:-1]
        #img = PIL.Image.open(img_path + image)
        #os.makedirs(f'../../Data/ChestXRay/images/', exist_ok=True)
        os.makedirs(f'../../Data/ChestXRay/labels/', exist_ok=True)
        #img.save(f'../../Data/ChestXRay/images/{split}/{image}')
        with open(f'../../Data/ChestXRay/labels/{image.split(".")[0]}.txt', 'w') as file:
            file.writelines(lines)

    images = [f'./images/{s}' + '\n' for s in df['image'].unique()]
    images[-1] = images[-1][:-1]

    with open(f'../../Data/ChestXRay/{split}.txt', 'w') as file:
        file.writelines(images)


# In[50]:


prepare_data(val_df, 'val')
prepare_data(train_df, 'train')
prepare_data(test_df, 'test')


# In[ ]:





# In[53]:


boxes.to_csv('../../Data/ChestXRay/boxes.csv', index=False)


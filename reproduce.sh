python prepare_cxr8data.py
python prepare_vinbigdata.py
python combine_data.py

python train_dual.py --workers 32 --device 0 --batch 56 --data data/combined_data.yaml --img 320 --cfg models/detect/yolov9-s.yaml --weights weights/yolov9-s.pt --name __fulldata_yolov9s_nobal_img320_bs56_clsmaug2_singlecls --hyp cls_maug_2.yaml --epochs 100 --optimizer SGD --cos-lr --patience 0 --exist-ok --single-cls
python train_dual.py --workers 32 --device 0 --batch 56 --data data/combined_data.yaml --img 320 --cfg models/detect/yolov9-s.yaml --weights weights/yolov9-s.pt --name __fulldata_yolov9s_nobal_img320_bs56 --hyp cls_maug.yaml --epochs 100 --optimizer SGD --cos-lr --patience 0 --exist-ok

python val_dual.py --name prod_val --conf-thres 0.7 --iou-thres 0.3 --data data/chestxray.yaml --weights runs/train/__fulldata_yolov9s_nobal_img320_bs56_clsmaug2_singlecls/weights/last.pt --batch-size 16 --img-size 320 --task test --device cpu --workers 24 --verbose --save-json --single-cls
python val_dual.py --name prod_val --conf-thres 0.5,0.6,0.8,0.55,0.65,0.45,0.5 --iou-thres 0.3 --data data/chestxray.yaml --weights runs/train/__fulldata_yolov9s_nobal_img320_bs56/weights/last.pt --batch-size 16 --img-size 320 --task test --device cpu --workers 24 --verbose --save-json

python export.py --data data/chestxray.yaml --weights runs/train/__fulldata_yolov9s_nobal_img320_bs56_clsmaug2_singlecls/weights/last.pt --img-size 320 --batch-size 1 --device cpu --iou-thres 0.3 --conf-thres 0.7 --include onnx
python export.py --data data/chestxray.yaml --weights runs/train/__fulldata_yolov9s_nobal_img320_bs56/weights/last.pt --img-size 320 --batch-size 1 --device cpu --iou-thres 0.3 --conf-thres 0.45 --include onnx
